// el require es para traerme la funcion express que está dentro de la carpeta node_modules,
// aunque lo he instalado, necesito hacer esto para usarlo
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.listen(port);
console.log("api escuchando el puerto " + port);


app.get('/apitechu/v1/hello',
//funcion request and response
  function (req,res) {
    console.log("GET /apitechu/v1/hello");

    //res es la respuesta
    res.send({"msg" :"Hola desde ApiTechU!!!"});
  }
);

app.get('/apitechu/v1/users',
//funcion request and response
function(req, res) {
       console.log("GET /apitechu/v1/users");
       console.log(req.query);

       var result = {};
       var users = require('./usuarios.json');

       if (req.query.$count == "true") {
         console.log("Count needed");
         result.count = users.length;
       }

       result.users = req.query.$top ?
       users.slice(0, req.query.$top) : users;

       res.send(result);

       //dirname indica que es el directorio donde estoy
   //    res.sendFile('usuarios.json', {root: __dirname});

       //var users = require('./usuarios.json');
       //res.send(users);
     }
    );
app.get('/apitechu/v1/users2',
function(req, res) {
       console.log("GET /apitechu/v1/users2");
       var users2 = require('./usuarios2.json');
       res.send(users2);
     }
);




app.post('/apitechu/v1/users',
  function (req,res) {
    console.log("first_name es " + req.body.first_name);
    console.log("last_name es " + req.body.last_name);
    console.log("email es " + req.body.email);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
    };

    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataFile(users);
    console.log("Usuario añadido con éxito");
    //TRAMPA PARA VER SI SE HA CARGADO EL USUARIO, PERO ESTO NO SE HACE...
    res.send({"msg" : "Usuario añadido con éxito"});
}
);
// el :id es un parametro
app.delete('/apitechu/v1/users/:id',
  function (req,res) {
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("id es " + req.params.id);

    var users = require('./usuarios.json');
//    users.splice(req.params.id - 1, 1);
    var idrecibida = req.params.id;
    var index = 0;

    for (user of users){
      console.log("objeto; " + user);
      console.log("idobjeto" + user.id);
      console,log("Length of array is " + users.length);
      if (user != null && user.id == idrecibida){
        delete users[index];
        writeUserDataFile(users);
        console.log("Usuario borrado");
        res.send({"msg" : "Usuario borrado"});
        break;
      }else{
        index++;
        console.log("idfichero; " + index);
      }
    }
  }
);

function writeUserDataFile(data) {
  const fs =require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios2.json", jsonUserData, "utf8",
  function(err) {
      if (err) {
      console.log(err);
      } else {
      console.log("Datos escritos en fichero.");
      }
    }
  )
}

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query Sring");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);


app.post('/apitechu/v1/loging',
  function (req,res) {
    console.log("POST /apitechu/v1/users");
    console.log("email es " + req.body.email);
    console.log("PW es "    + req.body.password);
    var users = require('./usuarios2.json');
    var finded = false;

    for (user of users){
      console.log(user.password);
      console.log(req.body.password);
      if ((user != null && user.email == req.body.email) && (user.password == req.body.password)){
        console.log("entro en el IF222");
            finded = true;
            user.logged = true;
            res.send("Loging correcto");
          }
    }
    if (finded != true){
      res.send("Loging incorrecto");
  }
}
);

app.post('/apitechu/v1/logout',
  function (req,res) {
    console.log("POST /apitechu/v1/users");
    console.log("id recibido " + req.body.id);
    var users = require('./usuarios2.json');
    var finded = false;
    for (user of users){
      console.log("objeto; " + user);
      console.log("posicion del array " + user.id);
      if ((user != null && user.id == req.body.id) && (user.logged == true)){
        console.log("entro en el IF");
        finded = true;
        delete user.logged;
        res.send("Logout correcto");
      }
    }
    if (finded != true){
      res.send("Logout incorrecto");
    }
  }
);
